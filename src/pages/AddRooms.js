import {Form, Container, Button, Row, Col} from 'react-bootstrap';
import {Fragment, useState, useEffect,useContext} from 'react';
import Swal from 'sweetalert2';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';



export default function AddRooms() {
const [name,setName] = useState('');
const [description, setDescription] = useState('');
const [price, setPrice] = useState('');
const [imgLink, setImgLink] = useState('');
const [isActive, setIsActive] = useState(false);

const {user, setUser} = useContext(UserContext);

const navigate = useNavigate();

  useEffect(() =>{
    
    if (name !== "" && description !== "" && price !== "" && imgLink !== "" && price >= 1){
      setIsActive(false);
    }
    else{
      setIsActive(true);
    }
  }, [name,description, price, imgLink])

function addRooms(event){
  event.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/room/create`,{
    method : 'POST',
    headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body:JSON.stringify({
        name : name,
        description : description,
        price: price,
        imgLink : imgLink
      })
  })
  .then(result => result.json())
  .then(data => { 


      if (data) {

        Swal.fire({
            title: 'Item was successfully registered',
            text: `Name: ${name} \nDescription: ${description} \nPrice: ${price}`,
            imageUrl: `${imgLink}`,
            imageHeight: 200,
            imageWidth: 300,
            
        })
          setName('');
          setDescription('');
          setPrice('');
          setImgLink('');
          
      }
      else{
        Swal.fire({
          title: "Item registration unsuccessful.",
          icon: "error",
          text: "Please check the details or contact your administrator."

        })


      }

  })






}



  return (
  
    user && user.isAdmin? 
    
    <Fragment>
      <Container>
      <Form className="mb-3 mt-5" onSubmit ={event =>addRooms(event)} autoComplete="off">
        <Form.Group>
          <Form.Label>Item Name</Form.Label>
          <Form.Control
            placeholder="Item Name"
            value ={name}
            aria-describedby="basic-addon1"
            className="mb-3"
            onChange ={event=> setName(event.target.value)}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Description</Form.Label>
          <Form.Control 
            as="textarea" aria-label="With textarea"
            placeholder="Tell us about the item"
            value ={description}
            aria-describedby="basic-addon2"
            className="mb-3"
            onChange ={event=> setDescription(event.target.value)}
          />
        </Form.Group>
          
          
        <Form.Group>
        <Row>
          <Col className=" col-6">
            <Form.Label>Price</Form.Label>
            <Form.Control
              placeholder="₱"
              value ={price}
              aria-describedby="basic-addon3"
              className="mb-3"
              onChange ={event=> setPrice(event.target.value)}
            />
        </Col>
        <Col className=" col-6">
            <Form.Label>Image Link</Form.Label>
            <Form.Control
              placeholder="www.drive.google.com"
              value ={imgLink}
              aria-describedby="basic-addon4"
              className="mb-3"
              onChange ={event=> setImgLink(event.target.value)}
            />
        </Col>
        </Row>
        </Form.Group>
        
        <Button variant="primary" type="submit" disabled={isActive}>
          Done
        </Button>

      </Form>
    </Container>
    </Fragment>
    :

    <Navigate to = "/*"/>

    
  );
}
