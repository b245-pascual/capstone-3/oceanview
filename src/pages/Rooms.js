import {Button, Form, Row, Col, Table, Container} from 'react-bootstrap';
import {Fragment, useContext, useState, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

import '../index.css';

import Rooms from '../components/Rooms.js'


import UserContext from '../UserContext.js';


export default function RoomsView() {

  const [name,setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [imgLink, setImgLink] = useState('');
  const [isHidden, setIsHidden] = useState(true);

  const {user} = useContext(UserContext);

  const [isActive, setIsActive] = useState(false);


  const[rooms, setRooms] = useState([]);

  useEffect(()=>{
    // fetch all products
    fetch(`${process.env.REACT_APP_API_URL}/room/allRooms`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
  
      // to change the value of our courses so we have to use the setCourses
      setRooms(data.map(room=>{
        return (<Rooms key ={room._id} roomsProp = {room}/>)
      }))
    })
  }, [])

  useEffect(() =>{
      
      if (name !== "" && description !== "" && price !== "" && imgLink !== "" && price >= 1){
        setIsActive(false);
      }
      else{
        setIsActive(true);
      }
    }, [name,description, price, imgLink])

 /* function addProduct(event){
  event.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/product/addProduct`,{
    method : 'POST',
    headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body:JSON.stringify({
        name : name,
        description : description,
        price: price,
        imgLink : imgLink
      })
  })
  .then(result => result.json())
  .then(data => { 


      if (data) {

        Swal.fire({
            title: 'Item was successfully registered',
            text: `Name: ${name} \nDescription: ${description} \nPrice: ${price}`,
            imageUrl: `${imgLink}`,
            imageHeight: 200,
            imageWidth: 300,
            
        })
          setName('');
          setDescription('');
          setPrice('');
          setImgLink('');
          
      }
      else{
        Swal.fire({
          title: "Item registration unsuccessful.",
          icon: "error",
          text: "Please check the details or contact your administrator."

        })


      }

  })

}*/


  return (
  
    user && user.isAdmin? 

    <Fragment>  
      <Container className="d-none d-lg-block">
      <h1 className = "text-center mt-5">Admin Dashboard</h1>
            <Table responsive="sm" striped bordered hover className="mt-5">
                 <thead>
                   <tr>
                     <th>ID</th>
                     <th>Name</th>
                     <th>Description</th>
                     <th>Price</th>
                     <th>Action</th>
                   </tr>
                 </thead>
                 <tbody>
                    {rooms}
                 </tbody>
              </Table>
      </Container>
    </Fragment>

    
    :

    <Navigate to = "/"/>

    
  );
}

