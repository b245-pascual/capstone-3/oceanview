import {Container, Nav, Navbar, NavDropdown} from 'react-bootstrap';
import {useContext, useState, Fragment} from 'react';

/*import { AiTwotoneBook } from 'react-icons/ai';*/

import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext.js'

export default function AppNavBar(){

  const {user} = useContext(UserContext);
  const [expanded, setExpanded] = useState(false);

  
  

  return (

    <Navbar className="shadow" bg="info" expand="lg" expanded={expanded}>
          <Container className="d-lg-none">
            <Navbar.Brand as = {Link} to = "/" >OceanView</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => setExpanded(!expanded)}/>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto">
                <Nav.Link as = {NavLink} to = "/" onClick={() => setExpanded(!expanded)}>Home</Nav.Link>
                <Nav.Link as = {NavLink} to = "/roomsearch"  onClick={() => setExpanded(!expanded)}>Rooms</Nav.Link>

            {
              user && user.isAdmin?

              <NavDropdown title="Dashboard" id="basic-nav-dropdown">
                       <NavDropdown.Item as = {NavLink} to = "/roomsView"  onClick={() => setExpanded(!expanded)}>View Rooms</NavDropdown.Item>
                       <NavDropdown.Item as = {NavLink} to = "/AddRooms"  onClick={() => setExpanded(!expanded)}>
                         Add Products </NavDropdown.Item>
                       <NavDropdown.Item as = {NavLink} to = "/"  onClick={() => setExpanded(!expanded)}>Account Control</NavDropdown.Item>
                         
              </NavDropdown>
              :
              null
            }

            {/*conditional rendering*/}
            {
              user?
              <Nav.Link as = {NavLink} to = "/logout"  onClick={() => setExpanded(!expanded)}>Logout</Nav.Link>
              :
              <Fragment>
                <Nav.Link as = {NavLink} to = "/register"  onClick={() => setExpanded(!expanded)}>Register</Nav.Link>
                  <Nav.Link as = {NavLink} to = "/login"  onClick={() => setExpanded(!expanded)}>Login</Nav.Link>
              </Fragment>
            }
              </Nav>
            </Navbar.Collapse>
          </Container>

          <Container className="d-none d-lg-flex">
           {/* <Navbar.Brand as = {Link} to = "/"><AiTwotoneBook size={30}className='align-middle'/> ebook</Navbar.Brand>*/}
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto">
                <Nav.Link as = {NavLink} to = "/" >Home</Nav.Link>
                <Nav.Link as = {NavLink} to = "/roomsearch" >Accomodation</Nav.Link>

            {
              user && user.isAdmin?

              <NavDropdown title="Dashboard" id="basic-nav-dropdown">
                       <NavDropdown.Item as = {NavLink} to = "/roomsView" >View Rooms</NavDropdown.Item>
                       <NavDropdown.Item as = {NavLink} to = "/AddRooms" > Add Rooms </NavDropdown.Item>
                       <NavDropdown.Item as = {NavLink} to = "*" > Settings</NavDropdown.Item>
                         
              </NavDropdown>
              :
              null
            }

            {/*conditional rendering*/}
            {
              user?
              <Nav.Link as = {NavLink} to = "/logout" >Logout</Nav.Link>
              :
              <Fragment>
                <Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
                  <Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
              </Fragment>
            }
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>


    )
}



