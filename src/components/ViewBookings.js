import {Button, Form, Row, Col, Table, Container} from 'react-bootstrap';
import {Fragment, useContext, useState, useEffect} from 'react';
import {Navigate, Link, useParams} from 'react-router-dom';
import Swal from 'sweetalert2';

import {CgShoppingCart} from  'react-icons/cg'


import '../index.css';

import OrderCartItems from '../components/OrderCartItems.js'


import UserContext from '../UserContext.js';


export default function ViewOrder() {

  const {orderId} = useParams();

  const[orderCartItems, setOrderCartItems] = useState([]);
  const[totalAmount, setTotalAmount] = useState();
  const[completed, setCompleted] = useState(false);
  const {user} = useContext(UserContext);

  function display () {
    fetch(`${process.env.REACT_APP_API_URL}/order/reviewOrder/${orderId}/view`,{
      method: 'PUT',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(result=>result.json())
    .then(data => {
      setCompleted(data.completed)
    setTotalAmount(data.totalAmount) 
      // to change the value of our courses so we have to use the setCourses
      setOrderCartItems(data.products.map(product=>{
        return (<OrderCartItems key ={product.productId} orderCartProp = {product}/>)
      }))
    })
  }


  useEffect(()=>{
    // fetch all products
    display()
  }, [])


  return (
  
    user && !user.isAdmin? 

    <Fragment>  
      <Container className="mt-3">
            <h2><CgShoppingCart/></h2>
            <Table responsive="sm" striped bordered hover className="mt-2 shadow">
                 <thead>
                   <tr>
                     <th>Name</th>
                     <th>Description</th>
                     <th>Price</th>
                     <th>Qty</th>
                     <th hidden>Action</th>
                   </tr>
                 </thead>
                 <tbody>
                  {orderCartItems}
                 </tbody>
              </Table>
              <div className="text-center">
              <h6 className="pt-3 px-2">Total: PHP{totalAmount}</h6>

             
              <Button variant="light shadow" type="submit" disabled> Completed </Button>
              
              </div>
          
                
      </Container>
    </Fragment>

    
    :

    <Navigate to = "/productsearch"/>

    
  );
}
