import {Table, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {AiOutlineEdit} from  'react-icons/ai'
import {BiArchiveIn} from  'react-icons/bi'
import {MdUnarchive} from  'react-icons/md'







export default function Rooms({roomsProp}) {
  
  const{_id, name, description, price, isActive } = roomsProp;
 

  return (
    
        <tr key={_id}>
          <td>{_id}</td>
          <td>{name}</td>
          <td>{description}</td>
          <td>{price}</td>
          <td>
                  <Link to = {`/updateRooms/${_id}`}  className="text-dark">Edit</Link>/

                  {
                     isActive?
                    <Link to ={`/archive/${_id}/false`}className="text-dark"><BiArchiveIn/></Link>
                    :
                    <Link to ={`/archive/${_id}/true`}className="text-dark"><MdUnarchive/></Link>
                  }
                  

          </td>
        </tr>
      
  );
}




 
